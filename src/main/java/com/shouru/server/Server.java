package com.shouru.server;

public interface Server {
	public void start();
	public void stop();
	/**
	 * 设置服务器端口
	 * @param port
	 * @create_date 2016年7月11日
	 */
	public Server port(int port);
	/**
	 * 设置服务器根路径
	 * @param webAppPath
	 * @create_date 2016年7月11日
	 */
	public Server webAppDir(String dir);
	/**
	 * 设置修改扫描间隔时间
	 * @param i
	 * @create_date 2016年7月11日
	 */
	public Server scanIntervalSeconds(Long scanIntervalSeconds);
	
	/**
	 * 增加不检测修改的路径<br/>
	 * eg:<br/>
	 *  /public/**  网站根目录下public目录下的所有文件<br/> 
	 *  /res/** 网站根目录下res目录下的所有文件<br/> 
	 *  /** /*.jsp 所有jsp后缀的文件<br/> 
	 * @param antPattern 
	 * @create_date 2016年7月12日
	 */
	public Server addExcludeScanPath(String antPattern); 
	/**
	 * 热启动触发文件<br/>
	 * 如果设置了该文件，仅有该文件改变时才会热启动。<br/>
	 * 此功能可以<b>精确地</b>控制热启动的时间<br/>
	 * @param path
	 * @return
	 * @create_date 2016年7月12日
	 */
	public Server triggerFile(String path);

}
