#inServer

内置Web服务器，实现以main方法启动web项目，自动检测代码修改并热部署

###传统web项目开发部署方式对比：
    - 项目不再要求是web项目，可以是java se项目，maven simple project
    - PC上不必安装tomcat等服务器
    - 可以控制某些目录下的文件不引起reload
    - 可以主动控制热部署的时间点，eclipse 的server 插件会在任何文件修改后reload，本项目可以实现在所有文件修改完毕后再reload（trigger file 功能）

###功能：

1. 使Web项目无需部署到外置服务器，只需以**main方法启动**即可，节省频繁部署的时间。
2. 拥有自动检测代码修改并自动热部署功能。
3. 如果项目依赖于其他项目，会**连动检测依赖项目或类库**的修改（新增依赖时目前无法检测到）。
4. 可以设置**不检测的路径**，比如静态资源路径，通过Ant path格式设置。
5. 可以设置**热部署触发文件**，设置后只有此文件被修改后才会热部署，此功能可准确的控制热部署的时间。


> 本项目借鉴了JFinal内部的JettyServer,在此膜拜一下@JFinal
> 功能 3,4,5在JFinal中并没有实现，此项目做了补充


###使用说明：
- 加入依赖 
> scope 推荐使用provided，可以在打包时不打包此依赖。
> 暂时没有发布到公共仓库，请自行添加到私有仓库。

- 新建一个包含main方法的类，在main方法如下：

```
	public static void main(String[] args) {
		Servers.getServer().start();
	}
```

当输出类似文字，则服务器启动成功：	
```
服务器启动成功:
	 端口：80
	 Web根目录：src/main/webapp
	 修改扫描间隔：5
```
	
###配置：
- 设置服务器端口,默认8000

```
Servers.getServer().port(8000).start();
```
	
- 设置项目根目录，默认"src/main/webapp"

```
Servers.getServer().webAppDir("src/main/webRoot").start();
```
	
- 设置修改检测间隔,默认5秒

```
Servers.getServer().scanIntervalSeconds(3L).start();
```
	
- 增加修改检测排除路径，匹配该路径的所有文件不进行更改检测， 该路径使用Ant path风格
	已内置路径：/static/**

```
Servers.getServer().addExcludeScanPath("/public/**").start();
```
	
- 设置热启动触发文件，
    如果设置了该文件，仅有该文件改变时才会热启动。
    此功能可以精确地控制热启动的时间点
    默认无

```
Servers.getServer().triggerFile("/static/restart.xml").start();
```

###综合设置：
```
Servers
	.getServer()
	.port(8888)
	.scanIntervalSeconds(3L)
	.triggerFile("/test/test.xml")
	.start();
```